﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 10f;
    //public Slider Shrinkmeter;
    public float movespeed = 2.5f;
    public Slider Sprintmeter;
    public Image ShrinkCooldown;
    public bool itemCollected;
    public bool PlayingGame;

    public Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    bool shrunk;
    float Maxshrinkenergy = 5f;
    float shrinkenergy;

    float sprintenergy = 1f; //how much sprint we have
    float Maxsprintenergy = 1f;

    void Start()
    {
        movespeed = 3f;
        Sprintmeter.value = calculatedsprintenergy();

        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        shrinkenergy = Maxshrinkenergy;

        ShrinkCooldown.fillAmount = calculatedshrinkenergy();
    }

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Q) && shrinkenergy >= Maxshrinkenergy)
        {
            shrunk = true;
            transform.localScale = Vector3.one * .25f;
        }

        if (shrunk)
        {
            shrinkenergy -= Time.deltaTime;
            ShrinkCooldown.fillAmount = calculatedshrinkenergy();
            //m_Animator.speed = 10f;
            if (shrinkenergy < 0f)
            {
                shrunk = false;
                transform.localScale = Vector3.one;
            }
        }
        else
        {
            shrinkenergy += Time.deltaTime * .5f;
            ShrinkCooldown.fillAmount = calculatedshrinkenergy();
            if (shrinkenergy > Maxshrinkenergy)
                shrinkenergy = Maxshrinkenergy;
        }
        
        
        if (Input.GetKey(KeyCode.LeftShift) && sprintenergy > 0f)
        {
            movespeed = 6f;
            sprintenergy -= Time.deltaTime;
            Sprintmeter.value = calculatedsprintenergy();
        }
        else
        {
            movespeed = 3f;
            sprintenergy += Time.deltaTime * .25f;
            Sprintmeter.value = calculatedsprintenergy();
            if (sprintenergy > Maxsprintenergy)
                sprintenergy = Maxsprintenergy;

        }
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        //m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
        m_Rigidbody.MovePosition(m_Rigidbody.position + (transform.TransformDirection(m_Movement) * movespeed * Time.deltaTime)); //checking animation for movement
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag ("Treasure"))
        {
            other.gameObject.SetActive (false);
            itemCollected = true;
            Debug.Log("Got Treasure!");
        }

    }

    float calculatedshrinkenergy()
    {
        return shrinkenergy / Maxshrinkenergy;
    }

    float calculatedsprintenergy()
    {
        return sprintenergy / Maxsprintenergy;
    }

    



}