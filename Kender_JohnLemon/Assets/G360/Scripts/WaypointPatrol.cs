﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public AudioSource seenAudio;
    public AudioSource hiddenAudio;

    Animator m_Animator;
    Transform player;
    bool aggro;
    //bool isaudioplaying;
    //bool m_HasAudioPlayed;

    public GameEnding gameEnding;

    int m_CurrentWaypointIndex;

    void Start()
    {
        m_Animator = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        navMeshAgent.SetDestination (waypoints[0].position);
    }

    void Update()
    {
        if (!aggro && navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination (waypoints[m_CurrentWaypointIndex].position);
        }

    }

    public void SetAggro(bool set)
    {
        aggro = set;
        if (aggro)
        {
            navMeshAgent.SetDestination(player.position);
        }

        else
        {
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
            
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.other.transform == player)
            gameEnding.CaughtPlayer();
        
    }
}
