﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public AudioSource hiddenAudio;
    public AudioSource seenAudio;
    public Transform player; //way to decect the player
    //public GameEnding gameEnding; // game ending varible //move to patrol

    bool m_IsPlayerInRange; // is the player detected or not

    public WaypointPatrol patrolScript;
    float aggroCooldown;

    private void Start()
    {
        hiddenAudio.Play();
    }
    void OnTriggerEnter(Collider other) //has something hit the gargoyle
    {
        if (other.transform == player) //if the player hits the gargoyle..
        {
            m_IsPlayerInRange = true; //...then that means the player is detected
            seenAudio.Play();
            hiddenAudio.Pause();
        }
    }

    void OnTriggerExit(Collider other) // is nothing colliding
    {
        if (other.transform == player) //if the player is not colliding
        {
            m_IsPlayerInRange = false; // then not detected player
            patrolScript.SetAggro(false);
            seenAudio.Stop();
            hiddenAudio.UnPause();
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange) //check gargoyle line of sight
        {
            Vector3 direction = player.position - transform.position + Vector3.up; //vec3.up is short for (0,1,0) new vec3 that is direction and sets it equal to distance between player and gargoyle
            Ray ray = new Ray(transform.position, direction); // ray checks in line 
            RaycastHit raycastHit; //did ray get hit

            if (Physics.Raycast(ray, out raycastHit)) //adds the physics to the ray so it goes straight out
            {
                if (raycastHit.collider.transform == player) // if the player hits the rays     
                {
                    patrolScript.SetAggro(true);

                    //gameEnding.CaughtPlayer();

                }
            }
        }
    }
}