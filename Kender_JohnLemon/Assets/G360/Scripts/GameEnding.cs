﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f; //duration for the fade of the UI to come in and out
    public float displayImageDuration = 1f; //
    public GameObject player; // so that it can detect Lemon when he hits the collider
    public CanvasGroup exitBackgroundImageCanvasGroup;//variable in the inspector to effect the Canvas Group
    public AudioSource exitAudio; //win audio variable
    public CanvasGroup caughtBackgroundImageCanvasGroup; // for the ending UI
    public AudioSource caughtAudio; //when ya get caught sound
    public PlayerMovement pMovement;
    public AudioSource seenAudio;
    public AudioSource hiddenAudio;
    public GameObject Sprint;
    public GameObject Shrink;
    public GameObject Minimap;

    bool m_IsPlayerCaught; // did the player get caught?
    bool m_IsPlayerAtExit;  //true or false did the player get to the exit
    float m_Timer; // a timer so that end screen durations go all the way through
    bool m_HasAudioPlayed; //is the audio playing

    void OnTriggerEnter(Collider other) //if something collides with these collider
    {
        if (other.gameObject == player) //player mr lemon runs into the collider
        {
            m_IsPlayerAtExit = true; //the bool is true here
        }
    }
    public void CaughtPlayer () //create public to set in unity
    {
        m_IsPlayerCaught = true; //this bool is true here
        seenAudio.Stop();
        hiddenAudio.Stop();
    }
    void Update()
    {

        if (m_IsPlayerAtExit && pMovement.itemCollected)//did they make it ?
        {
           hiddenAudio.Stop();
           Sprint.SetActive(false);
           Shrink.SetActive(false);
           Minimap.SetActive(false);
           Cursor.lockState = CursorLockMode.None;
            EndLevel (exitBackgroundImageCanvasGroup,false,exitAudio);// when the player wins the game 

        }
        else if (m_IsPlayerCaught) //didn't make it
        {
            Sprint.SetActive(false);
            Shrink.SetActive(false);
            Minimap.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            EndLevel (caughtBackgroundImageCanvasGroup, true,caughtAudio); //show lost game UI

        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource) //bring up the image is endlevel happens
    {
        if (!m_HasAudioPlayed) //check to see if the music has played so it only plays once
        {
            audioSource.Play(); //play the sound
            m_HasAudioPlayed = true; //bool set to true
        }
        m_Timer += Time.deltaTime; // set the time to self

        imageCanvasGroup.alpha = m_Timer / fadeDuration; //again fading in timer

        if (m_Timer > fadeDuration + displayImageDuration) //display image for the fade duration
        {
            if (doRestart) // if the restart is true
            {
                SceneManager.LoadScene("CaughtMenu"); //reload the game from beginning
            }
            else
            {
                Application.Quit(); //end game
            }
        }
    }
}